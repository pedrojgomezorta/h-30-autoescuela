<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>H30 Autoescuela - Inicio</title>
        <meta name="description" content="Empresa familiar dedicada a la formación vial en Cartaya (Huelva) donde podrás sacarte el carnet de conducir.">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <meta name="keywords" content="carnet, autoescuela, Cartaya, formación vial, negocios">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 element-animation">
                        <h1><img src="img/logo.png"></h1>
                    </div>
                    <div class="centrar hidden-xs col-sm-6 col-md-4 col-lg-4">
                        <h1 align="right">H-30 Autoescuela</h1>
                        <h5>C/Palmera N33 - 21450 Cartaya (Huelva) - Tlf. 656 97 33 28</h5>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span href="#" class="navbar-brand">Menú</span>
                </div>
                <div class="row">
                    <div class="collapse navbar-collapse navbar-ex1-collapse col-xs-12 col-sm-10 col-md-10 col-lg-10">
                        <ul class="nav navbar-nav">
                            <li class="selected"><a href="index.php" title="Pequeña galería junto a los servicios que ofrecemos">Inicio</a></li>
                            <li><a href="noticias.php" title="Conoce las últimas ofertas, actualizaciones, etc.">Noticias</a></li>
                            <li><a href="examen.php" title="Entérate de cual ha sido tu calificación en el examen teórico">¿Has aprobado?</a></li>
                            <li><a href="social.php" title="¿Qué está pasando en las redes sociales? Enterate de lo que pasa en Facebook, Twitter y Google+">Social</a></li>
                            <li><a href="contacto.php" title="¡Contactanos!">Contacto</a></li>
                        </ul>
                    </div>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2 redes">
                        <a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Flocalhost%2Fh30autoescuela.com%2Findex.php&region=follow_link&screen_name=H30Autoescuela&tw_p=followbutton" class="" data-show-count="false"><span id="twitter"></span></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        <a href="https://www.facebook.com/pages/H-30-Autoescuela/586958204766112?ref=ts&fref=ts" target="_blank"><span id="facebook"></span></a>
                        <a href="https://plus.google.com/b/110003763114806812190/110003763114806812190/posts" target="_blank"><span id="google"></span></a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <section id="slider" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#slider" data-slide-to="0" class="active"></li>
                    <li data-target="#slider" data-slide-to="1"></li>
                    <li data-target="#slider" data-slide-to="2"></li>
                    <li data-target="#slider" data-slide-to="3"></li>
                    <li data-target="#slider" data-slide-to="4"></li>
                    <li data-target="#slider" data-slide-to="5"></li>
                </ol>
                <div class="carousel-inner carousel-h30">
                    <div class="item active">
                        <img src="img/slider/4.jpg" class="adaptar">
                    </div>
                    <div class="item">
                        <img src="img/slider/2.jpg" class="adaptar">
                    </div>
                    <div class="item">
                        <img src="img/slider/3.jpg" class="adaptar">
                    </div>
                    <div class="item">
                        <img src="img/slider/1.jpg" class="adaptar">
                    </div>
                    <div class="item">
                        <img src="img/slider/5.jpg" class="adaptar">
                    </div>
                    <div class="item">
                        <img src="img/slider/6.jpg" class="adaptar">
                    </div>
                </div>
                <a href="#slider" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a href="#slider" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </section><br>
            <section>
                <div class="row serv">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3>Servicio:</h3><span class="glyphicon glyphicon-thumbs-up" align="right"></span>
                        <p> - Periodo de aprendizaje <b>fácil y rápido</b></p>
                        <p> - Con <b>precios económicos y pago accesible</b></p>
                        <p> - Servicio personalizado</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3>Tecnología:</h3><span class="glyphicon glyphicon-ok-sign" align="right"></span>
                        <p> - Puesto alumno ergonómico y confortable.</p>
                        <p> - Programa de tests <b>Visual Aeol</b> Nº1 en España</p>
                        <p> - Programa de explicación (profesor) dinámico</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3>Buscamos:</h3><span class="glyphicon glyphicon-search" align="right"></span>
                        <p> - Aprobado del <b>100%</b> del alumnado</p>
                        <p> - Comodidad y cercanía</p>
                        <p> - Calidad en la enseñanza y formar al futuro conductor</p>
                    </div>
                </div>
            </section>
        </div>
        <br>
        <footer class="color-rojo">
        <div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="img/logo-h-30.png" class="img-responsive">
                </div>
            </div>
        </div>
        </footer>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        © H-30 AUTOESCUELA - C/Palmera Nº33 - 21450 Cartaya (Huelva) - Tlf. 656973328
                    </div>
                    <div class="col-lg-4">
                        <a href="index.php">Inicio</a>
                        <a href="noticias.php">Noticias</a>
                        <a href="examen.php">¿Has aprobado?</a>
                        <a href="contacto.php">Contacto</a>
                    </div>
                </div>
            </div>
        </footer>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
