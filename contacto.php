<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>H30 Autoescuela - Contacto</title>
        <meta name="description" content="Empresa familiar dedicada a la formación vial en Cartaya (Huelva) donde podrás sacarte el carnet de conducir.">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <meta name="keywords" content="carnet, autoescuela, Cartaya, formación vial, negocios">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 element-animation">
                        <h1><img src="img/logo.png"></h1>
                    </div>
                    <div class="centrar hidden-xs col-sm-6 col-md-4 col-lg-4">
                        <h1 align="right">H-30 Autoescuela</h1>
                        <h5>C/Palmera N33 - 21450 Cartaya (Huelva) - Tlf. 656 97 33 28</h5>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand">Menú</span>
                </div>
                <div class="row">
                    <div class="collapse navbar-collapse navbar-ex1-collapse col-xs-12 col-sm-10 col-md-10 col-lg-10">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php" title="Pequeña galería junto a los servicios que ofrecemos">Inicio</a></li>
                            <li><a href="noticias.php" title="Conoce las últimas ofertas, actualizaciones, etc.">Noticias</a></li>
                            <li><a href="examen.php" title="Entérate de cual ha sido tu calificación en el examen teórico">¿Has aprobado?</a></li>
                            <li><a href="social.php" title="¿Qué está pasando en las redes sociales? Enterate de lo que pasa en Facebook, Twitter y Google+">Social</a></li>
                            <li class="selected"><a href="contacto.php" title="¡Contactanos!">Contacto</a></li>
                        </ul>
                    </div>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2 redes">
                        <a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Flocalhost%2Fh30autoescuela.com%2Findex.php&region=follow_link&screen_name=H30Autoescuela&tw_p=followbutton" class="" data-show-count="false"><span id="twitter"></span></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        <a href="https://www.facebook.com/pages/H-30-Autoescuela/586958204766112?ref=ts&fref=ts" target="_blank"><span id="facebook"></span></a>
                        <a href="https://plus.google.com/b/110003763114806812190/110003763114806812190/posts" target="_blank"><span id="google"></span></a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <section id="form">
                <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <h2>¡Contáctanos!</h2>
                    <form role="form" action="" method="post">
                        <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="nombre" name="nombre" class="form-control" id="nombre" placeholder="Inserta tu nombre">
                        </div>
                        <div class="form-group">
                        <label for="email">Correo</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Inserta tu correo">
                        </div>
                        <div class="form-group">
                        <label for="asunto">Asunto</label>
                        <input type="asunto" name="asunto" class="form-control" id="asunto" placeholder="Asunto">
                        </div>
                        <div class="form-group">
                            <label for="mensaje">Mensaje</label>
                            <textarea class="form-control" name="mensaje" id="mensaje" rows="4"></textarea>
                        </div> 
                        <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-default" value="Enviar">
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3174.6610895368362!2d-7.153010599999999!3d37.279463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd103246b09839c7%3A0x288302ce76cf14da!2sCalle+Palmera%2C+33!5e0!3m2!1ses!2ses!4v1397336751151" width="100%" height="400" frameborder="0" style="border:0"></iframe>
                </div>
                </div>
            </section>
            <section id="form">
                <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="contacto">
                        <div class="row">
                        <h1>Contacto</h1>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Teléfonos:</p>
                            <p class="c-img">656973328</p>
                            <p>656973327</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Correo:</p>
                            <p><a href="mailto:info@h30autoescuela.com"></a>info@h30autoescuela.com</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Dirección:</p>
                            <p>C/Palmera N33 - 21450 Cartaya (Huelva)</p>
                        </div>
                    </div>
                </div>
                </div>
            </section>
            <br>
        </div>
        <footer class="color-rojo">
        <div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="img/logo-h-30.png" class="img-responsive">
                </div>
            </div>
        </div>
        </footer>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        © H-30 AUTOESCUELA - C/Palmera Nº33 - 21450 Cartaya (Huelva) - Tlf. 656973328
                    </div>
                    <div class="col-lg-4">
                        <a href="index.php">Inicio</a>
                        <a href="noticias.php">Noticias</a>
                        <a href="examen.php">¿Has aprobado?</a>
                        <a href="contacto.php">Contacto</a>
                    </div>
                </div>
            </div>
        </footer>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
<?php
if (isset($_REQUEST['enviar'])){
    $body = "
    <html>
    <head>
    <title>Contacto H-30</title>
    </head>
    <body style='background:#EEE; padding:30px;'>
    <h2 style='color:#767676;'>Contacto Web</h2>";

    $body .= "
    <strong style='color:#0090C6;'>Nombre: </strong>
    <span style='color:#767676;'>" . $_POST["nombre"] . "</span><br>";

    $body .= "
    <strong style='color:#0090C6;'>Email: </strong>
    <span style='color:#767676;'>" . $_POST["email"] . "</span><br>";

    $body .= "
    <strong style='color:#0090C6;'>Asunto: </strong>
    <span style='color:#767676;'>" . $_POST["asunto"] . "</span><br>";

    $body .= "
    <strong style='color:#0090C6;'>Mensaje: </strong>
    <span style='color:#767676;'>" . $_POST["mensaje"] . "</span><br>";


    $body .= "</body></html>";
    if(mail("h30autoescuela@gmail.com", "Web H-30 Autoescuela" , $body)){
    echo "<script type='text/javascript'>alert('¡Correo de ".$_POST["email"]." enviado correctamente!');</script>";
    }
    else
    {
    echo "<script type='text/javascript'>alert('¡Ha habido algún problema, inténtalo más tarde ;)!');</script>";
    }
}
?>