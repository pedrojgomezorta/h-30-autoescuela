<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>H-30 Autoescuela - Noticias</title>
        <meta name="description" content="Empresa familiar dedicada a la formación vial en Cartaya (Huelva) donde podrás sacarte el carnet de conducir.">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <meta name="keywords" content="carnet, autoescuela, Cartaya, formación vial, negocios">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!--Cambiar colores reproductor -->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 element-animation">
                        <h1><img src="img/logo.png"></h1>
                    </div>
                    <div class="centrar hidden-xs col-sm-6 col-md-4 col-lg-4">
                        <h1 align="right">H-30 Autoescuela</h1>
                        <h5>C/Palmera N33 - 21450 Cartaya (Huelva) - Tlf. 656 97 33 28</h5>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">Menú</a>
                </div>
                <div class="row">
                    <div class="collapse navbar-collapse navbar-ex1-collapse col-xs-12 col-sm-10 col-md-10 col-lg-10">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php" title="Pequeña galería junto a los servicios que ofrecemos">Inicio</a></li>
                            <li class="selected"><a href="noticias.php" title="Conoce las últimas ofertas, actualizaciones, etc.">Noticias</a></li>
                            <li><a href="examen.php" title="Entérate de cual ha sido tu calificación en el examen teórico">¿Has aprobado?</a></li>
                            <li><a href="social.php" title="¿Qué está pasando en las redes sociales? Enterate de lo que pasa en Facebook, Twitter y Google+">Social</a></li>
                            <li><a href="contacto.php" title="¡Contactanos!">Contacto</a></li>
                        </ul>
                    </div>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2 redes">
                        <a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Flocalhost%2Fh30autoescuela.com%2Findex.php&region=follow_link&screen_name=H30Autoescuela&tw_p=followbutton" class="" data-show-count="false"><span id="twitter"></span></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        <a href="https://www.facebook.com/pages/H-30-Autoescuela/586958204766112?ref=ts&fref=ts" target="_blank"><span id="facebook"></span></a>
                        <a href="https://plus.google.com/b/110003763114806812190/110003763114806812190/posts" target="_blank"><span id="google"></span></a>
                    </div>
                </div>
            </div>
        </nav>
        <section>
            <div class="container">
                <div class="jumbotron">
        <h1>H-30 sortea un teórico en Radio Cartaya.</h1>
        <p class="lead">Entre los oyentes de Radio Cartaya, se sorteará un teórico completamente grátis. Participa llamando de lunes a viernes a partir de las 11:30 horas. Tlf <span class="rojo">959 39 10 86</span></p>
        <p><center><iframe id="se" name="se" width="100%" height="120" frameborder="0" allowfullscreen="" scrolling="no" src="http://www.ivoox.com/player_ej_3805381_2_1.html?data=lJ2dl5icdY6ZmKiak5eJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfotrh0ZCpt8TpxtHOja2Rd5GfxcqYpcbWuMLtwpDg0dfYqcKf1tOYh5ebmMaZpJiSpJjWrcTjhpekjcrScYarpJK%2Fw8nNs4y3wtfhw97FcYarpJKh&"></iframe></center>
</p>
      </div>
                <br>
                <div class="row publicidad">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tres">
                        <center>
                            <p class="">
                                <span class="color-rojo">20/10/2014</span>
                                 - ¿Qué esta pasando en las redes sociales? Ahora en el apartado <a href="social.php" class="rojo">social</a> puedes estar informado en nuestra página de Facebook, Twitter y Google+</p>
                        </center>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <center><img src="img/social/smi.gif" class="img-responsive social" style="border:none;" title="¡Síguenos!"></center>
                    </div>
                </div>
                <br>
                <div class="row publicidad">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 uno">
                        <center><img src="img/folleto.png" class="img-responsive"></center>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 dos">
                        <center><span class="glyphicon glyphicon-arrow-left flecha"></span></center>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 tres">
                        <center>
                            <p class="oferta">
                                <span class="color-rojo">19/06/2014</span>
                                 - Busca un folleto como este y <b>¡aprovecha esta oferta para sacarte el carnet!</b>, si no lo tienes no te preocupes, pásate por nuestra autoescuela y entérate de <b>ofertas exclusivas para ti</b> ;).
                            </p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg6 margen-inf">
                        <p>
                        <span class="color-rojo">11/04/2014 </span> - ¡Ofertón de semana santa! !Matricula a 90€!
                        </p>
                        <p>
                        <span class="color-rojo">04/04/2014 </span> - ¡Ya tenemos los primeros 20 alumnos matriculados con teórico gratis!, pero no se preocupen, ¡Pronto saldrán nuevas ofertas!
                        </p>
                        <p>
                        <span class="color-rojo">31/03/2014 </span> - Empezamos esta semana con <b>clases teóricas de 18:30 a 20:00 de la tarde.</b> ¡No faltes! ¡Prohibido suspender! :P
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg6 margen-inf">
                        <p>
                        <span class="color-rojo">08/04/2014 </span> - Ven e infórmate de los <b>nuevos bonos para clases prácticas</b> con precios muy económicos.
                        </p>
                        <p>
                        <span class="color-rojo">22/03/2014 </span> - Después de una semana con nuevas matrículas, aún no hemos llegado a las 20 primeras. ¡¿A que esperas?! ¡Todavía puedes tener tu teórico gratis! :-)
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margen-inf">
                        <p>
                        <span class="color-rojo">17/03/2014 </span> - ¿Aún no tienes el carnet? ¡Ahora es tu oportunidad! Autoescuela H-30 ofrece la manera más <b>rápida, fácil y económica</b> de sacarte el carnet de conducir. ¡Apúntate!
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margen-inf">
                        <p>
                        <span class="color-rojo">17/03/2014 </span> - Abrimos nuestras puertas y <b>¡LOS 20 PRIMEROS ALUMNOS TIENEN EL TEÓRICO GRATIS!</b> * abonando sólo las tasas de tráfico. ¡Te esperamos!
                        </p>
                    </div>             
                </div>
            </div>
        </section>
        <footer class="color-rojo">
        <div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="img/logo-h-30.png" class="img-responsive">
                </div>
            </div>
        </div>
        </footer>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        © H-30 AUTOESCUELA - C/Palmera Nº33 - 21450 Cartaya (Huelva) - Tlf. 656973328
                    </div>
                    <div class="col-lg-4">
                        <a href="index.php">Inicio</a>
                        <a href="noticias.php">Noticias</a>
                        <a href="examen.php">¿Has aprobado?</a>
                        <a href="contacto.php">Contacto</a>
                    </div>
                </div>
            </div>
        </footer>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
