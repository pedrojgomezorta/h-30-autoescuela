$(document).ready(function() {
	$('input[name="crear"]').on('click', function(){
        $('span.load').html('<img src="spin.gif"/>'); 
        event.preventDefault();
        var alumno=new Object();
        alumno.n_inscripcion=$('input[name="n_inscripcion"]').val();
        alumno.nombre=$('input[name="nombre"]').val();
        alumno.apellidos=$('input[name="apellidos"]').val();
        alumno.DNI=$('input[name="DNI"]').val();
        alumno.domicilio=$('input[name="domicilio"]').val();
        alumno.localidad=$('input[name="localidad"]').val();
        alumno.provincia=$('input[name="provincia"]').val();
        alumno.fecha_nac=$('input[name="fecha_nac"]').val();
        alumno.fecha_inicio=$('input[name="fecha_inicio"]').val();          
        alumno.cp=$('input[name="cp"]').val();
        alumno.telefono=$('input[name="telefono"]').val();
        $.ajax({
            data: alumno,
            url: 'crear_alumno.php',
            type: 'post',
            success: function(response) {
                alert(response);
                parent.location.reload();
            },
            fail: function () {
                alert('No se ha podido acceder al servidor');
            }
        });
    });
    $('div.datos-alumnos').on('click', function(){
        $(this).find('.datos-usuarios').toggle(400);
    });
});