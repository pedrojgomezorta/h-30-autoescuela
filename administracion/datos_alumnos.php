<?php
session_start();
if(!isset($_SESSION['usuario'])) 
{
  header('Location: login.php'); 
  exit();
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>H30 Autoescuela - Administración</title>
        <meta name="description" content="Empresa familiar dedicada a la formación vial en Cartaya (Huelva) donde podrás sacarte el carnet de conducir.">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <meta name="keywords" content="carnet, autoescuela, Cartaya, formación vial, negocios">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
        <script src="../js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <div class="alta"></div>
    <div class="fondonegro"></div>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 element-animation">
                        <h1><img src="../img/logo.png"></h1>
                    </div>
                    <div class="centrar hidden-xs col-sm-6 col-md-4 col-lg-4">
                        <h1 align="right">H-30 Autoescuela</h1>
                        <h5>C/Palmera N33 - 21450 Cartaya (Huelva) - Tlf. 656 97 33 28</h5>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span href="#" class="navbar-brand">Menú</span>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse col-xs-12 col-sm-10 col-md-10 col-lg-10">
                    <ul class="nav navbar-nav">
                        <li class="logout"><a href="logout.php" title="Salir de la parte de administración">Cerrar sesión</a></li>
                        <li class="logout"><a href="index.php" title="Parte principal">Administración</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <?php
                include "funciones.php";
                conectar_bd();
                $consulta = "SELECT * FROM alumnos a INNER JOIN usuarios u ON a.n_inscripcion = u.n_inscripcion ORDER BY 1";
                $result = mysql_query ($consulta)
                    or die("Error en la consulta SQL");
                    $i=2;
                    while( $row = mysql_fetch_array ( $result )){
                    if ($i%2==0){
                        echo "<div class='row'>";
                    }
                        echo "<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6 datos-alumnos'>";
                        echo "<h1>", $row['n_inscripcion'], "</h1>";
                        echo "<p>", utf8_encode($row['nombre']), " ", utf8_encode($row['apellidos']), " - ", $row['DNI'], " - Nacimiento: ", date("d-m-Y",strtotime($row['fecha_nac'])), "</p>";
                        echo "<p>", utf8_encode($row['domicilio']), " - ", utf8_encode($row['localidad']), " (", utf8_encode($row['provincia']), ")", " - ", $row['cp'], "</p>";
                        echo "<p>Teléfono: ", $row['telefono'], "</p>";
                        echo "<div class='datos-usuarios'>
                            <p>Usuario: ".$row['usuario']."</p>
                            <p>Contraseña: ".$row['pass']."</p>
                            <p>Test realizados: ".$row['test_realizados']."</p>
                            <p class='verde'>Test aprobados: ".$row['test_aprobados']."</p>
                            <p class='rojo'>Test suspendidos: ".$row['test_suspendidos']."</p>
                        </div>";
                        echo "</div>";
                    if ($i%2!=0){
                        echo "</div>";
                    }
                    $i++;
                    }
            ?>
        </div>
        <br>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        © H-30 AUTOESCUELA - C/Palmera Nº33 - 21450 Cartaya (Huelva) - Tlf. 656973328
                    </div>
                    <div class="col-lg-4">
                        <a href="../index.php">Inicio</a>
                        <a href="../noticias.php">Noticias</a>
                        <a href="../examen.php">¿Has aprobado?</a>
                        <a href="../contacto.php">Contacto</a>
                    </div>
                </div>
            </div>
        </footer>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="../js/vendor/bootstrap.js"></script>
        <script src="../js/main.js"></script>
        <script src="js.js"></script>
    </body>
</html>
