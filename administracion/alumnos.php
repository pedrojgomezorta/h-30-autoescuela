<link rel="stylesheet" type="text/css" href="styles.css">
<script src="../js/vendor/modernizr-2.6.2.min.js"></script>
<meta charset="utf-8">
<div class="container">
	<form method="post">
		<label>Número de inscripción</label>
		<input type="text" name="n_inscripcion" placeholder="00000" maxlength="5"><span class="req" style="display: none; color: rgb(230,0,0);">Deben de ser números.</span>
		<br>
		<label>Nombre</label>
		<input type="text" name="nombre" placeholder="Sebastian">
		<br>
		<label>Apellidos</label>
		<input type="text" name="apellidos" placeholder="Gómez Morgado">
		<br>
		<label>DNI</label>
		<input type="text" name="DNI" placeholder="00000000A" maxlength="9">
		<br>
		<label>Domicilio</label>
		<input type="text" name="domicilio" placeholder="C/Palmera Nº33">
		<br>
		<label>Localidad</label>
		<input type="text" name="localidad" value="Cartaya">
		<br>
		<label>Provincia</label>
		<input type="text" name="provincia" value="Huelva">
		<br>
		<label>Fecha de nacimiento</label>
		<input type="date" name="fecha_nac">
		<br>
		<label>Fecha inicio</label>
		<input type="date" name="fecha_inicio">
		<br>
		<label>Código postal</label>
		<input type="text" name="cp" value="21450" maxlength="5">
		<br>
		<label>Teléfono</label>
		<input type="text" name="telefono" placeholder="656973328" maxlength="9">
		<br>
		<input type="submit" name="crear" value="Crear"><span class="load"></span>
	</form>
</div>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="../js/vendor/bootstrap.js"></script>
<script src="../js/main.js"></script>
<script src="js.js"></script>